package com.example.androidjmdnstest;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.androidjmdnstest.inet.Connection;
import com.example.androidjmdnstest.inet.Connection.AfterConnectionAction;
import com.example.androidjmdnstest.inet.ReceiveMessage;

public class MainActivity extends ActionBarActivity {

	private Button mButtonStartServer;
	private Button mButtonConnect;
	private Button mButtonSend;
	private Button mButtonClose;
	private EditText text;
	public static Handler handler;
	private Context mContext;
	private Connection connection;
	private TextView outText;

	private AfterConnectionAction afterConnection = new AfterConnectionAction() {

		@Override
		public void AfterConnection() {
			handler.post(new Runnable() {
				@Override
				public void run() {
					outText.setText("���������� �����������");
				}
			});

		}
	};

	private ReceiveMessage receiveMessage = new ReceiveMessage() {
		@Override
		public void onReceive(final String msg) {
			handler.post(new Runnable() {
				@Override
				public void run() {
					outText.setText("�������� ���������: " + msg);
				}
			});
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mButtonStartServer = (Button) findViewById(R.id.createServButton);
		mButtonSend = (Button) findViewById(R.id.sendMessButton);
		mButtonConnect = (Button) findViewById(R.id.connectToServButton);
		mButtonClose = (Button) findViewById(R.id.closeAndExitButton);

		text = (EditText) findViewById(R.id.text);
		outText = (TextView) findViewById(R.id.textView2);

		mContext = this.getApplication().getApplicationContext();

		mButtonStartServer.setOnClickListener(mButtonStartServerOnClickListener);
		mButtonConnect.setOnClickListener(mButtonConnectOnClickListener);
		mButtonSend.setOnClickListener(mButtonSendMessageOnClickListener);
		mButtonClose.setOnClickListener(mButtonCorrectCloseOnClickListener);

		handler = new Handler();

	}

	private View.OnClickListener mButtonCorrectCloseOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {

			System.exit(0);
		};
	};

	private View.OnClickListener mButtonStartServerOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
			int intaddr = wifi.getConnectionInfo().getIpAddress();

			if (wifi.getWifiState() == WifiManager.WIFI_STATE_DISABLED || intaddr == 0) {
				startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
			} else {
				new AsyncTask<Void, Void, Void>() {
					@Override
					protected Void doInBackground(Void... params) {
						connection = Connection.createSetver(mContext, afterConnection);
						connection.receive(receiveMessage);
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						super.onPostExecute(result);
						text.setText("� ��� ������������");
					}
				}.execute((Void) null);
			}
		}
	};

	private View.OnClickListener mButtonConnectOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			new AsyncTask<Void, Void, Void>() {
				@Override
				protected Void doInBackground(Void... params) {
					connection = Connection.connectToServer(mContext, afterConnection);
					connection.receive(receiveMessage);
					return null;
				}

				@Override
				protected void onPostExecute(Void result) {
					super.onPostExecute(result);
				}
			}.execute((Void) null);

		}
	};

	private View.OnClickListener mButtonSendMessageOnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			try {
				connection.send(text.getText().toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		};
	};

}
