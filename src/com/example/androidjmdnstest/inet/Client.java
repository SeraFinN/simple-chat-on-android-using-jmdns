package com.example.androidjmdnstest.inet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
	private Socket cSocket = null;
	private PrintWriter out;
	
	public Client(InetAddress address, int port) {
		try {
			cSocket = new Socket(address, port);
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(cSocket.getOutputStream())), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void send(String msg) {
		if (out != null) {
			out.println(msg);
			out.flush();
		}
	}

	public void receive(ReceiveMessage receiveMessage) {
		BufferedReader in;
		try {
			in = new BufferedReader(new InputStreamReader(cSocket.getInputStream()));
			if (in != null) {
				while (true) {
					String msg = null;
					msg = in.readLine();
					receiveMessage.onReceive(msg);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	public void close() {
		try {
			if (cSocket != null)
				cSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Socket getSocket() {
		return cSocket;
	}
}
