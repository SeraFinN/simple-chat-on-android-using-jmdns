package com.example.androidjmdnstest.inet;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	private ServerSocket sServerSocket = null;
	private Socket sSocket = null;
	private int sPort = -1;
	private PrintWriter out;

	public Server() {
		try {
			sServerSocket = new ServerSocket(0);
			sPort = sServerSocket.getLocalPort();
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	public Server(int port) {
		try {
			sServerSocket = new ServerSocket(port);
			sPort = sServerSocket.getLocalPort();
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	public void createServer() {
		try {
			sSocket = sServerSocket.accept();
			if (sSocket != null) {
				out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(sSocket.getOutputStream())), true);
			}
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}

	public void send(String msg) {
		if (out != null) {
			out.println(msg);
			out.flush();
		}
	}
	public void receive(ReceiveMessage receiveMessage) {
		BufferedReader in;
		try {
			in = new BufferedReader(new InputStreamReader(sSocket.getInputStream()));
			if (in != null) {
				while (true) {
					String msg = null;
					msg = in.readLine();
					receiveMessage.onReceive(msg);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			close();
		}
	}
	public void close() {
		try {
			if (sServerSocket != null) {
				sServerSocket.close();
				sServerSocket = null;
			}
			if (sSocket != null){
				sSocket.close();
				sSocket = null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Socket getSocket() {
		return sSocket;
	}

	public int getPort() {
		return sPort;
	}

}
