package com.example.androidjmdnstest.inet;

import java.net.InetAddress;

import android.content.Context;

import com.example.androidjmdnstest.inet.NetworkDiscovery.OnFoundListenerAction;

public class Connection {

	public static enum TYPE_APPLICATION {
		SERVER, CLIENT
	};

	private Server server = null;
	private Client client = null;
	private AndroidNetworkDiscovery discovery = null;
	private AfterConnectionAction action = null;
	private ReceiveMessage receiveMessage = null;

	public static Connection createSetver(Context context, AfterConnectionAction afterConnection) {
		return new Connection(TYPE_APPLICATION.SERVER, context,afterConnection);
	}

	public static Connection connectToServer(Context context, AfterConnectionAction afterConnection) {
		return new Connection(TYPE_APPLICATION.CLIENT, context,afterConnection);
	}

	private Connection(TYPE_APPLICATION type, Context context, AfterConnectionAction afterConnection) {
		action = afterConnection;
		if (type == TYPE_APPLICATION.SERVER) {
			server = new Server();
			discovery = new AndroidNetworkDiscovery(context);
			discovery.reset();
			discovery.startService(server.getPort());
			server.createServer();
			if (action != null)
				action.AfterConnection();
		} else {
			discovery = new AndroidNetworkDiscovery(context);
			discovery.reset();
			discovery.listenServices(new OnFoundListenerAction() {

				@Override
				public void onFound(InetAddress address, int port) {
					client = new Client(address, port);
					System.out.println(address + ":" + port);
					if (action != null)
						action.AfterConnection();
					new Thread(new Runnable() {

						@Override
						public void run() {
							if (client != null && receiveMessage != null)
								client.receive(receiveMessage);
						}
					}).start();

				}
			});
		}
	}

	public void send(String msg) throws Exception {
		if (server != null)
			server.send(msg);
		else if (client != null)
			client.send(msg);
		else
			throw new Exception("��������� �� ����� ���� �����������, �.�. �� ����������� ����������");
	}

	public void receive(ReceiveMessage message) {
		receiveMessage = message;
		new Thread(new Runnable() {

			@Override
			public void run() {
				if (server != null)
					server.receive(receiveMessage);
			}
		}).start();
	}

	public static interface AfterConnectionAction {
		public void AfterConnection();
	}
}
