package com.example.androidjmdnstest.inet;

import java.io.IOException;
import java.net.InetAddress;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

public class NetworkDiscovery {
	protected JmDNS mJmDNS = null;
	private ServiceInfo mServiceInfo = null;
	private ServiceListener mServiceListener = null;
	
	public NetworkDiscovery() {
		try {
			mJmDNS = JmDNS.create();
		} catch (IOException e) {
			e.printStackTrace();
		}		

	}
	
	
	public void startService(int port) {
		mServiceInfo = ServiceInfo.create(Constants.TYPE, Constants.SERVICE_NAME, port, Constants.SERVICE_NAME);
		try {
			mJmDNS.registerService(mServiceInfo);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void listenServices(final OnFoundListenerAction listener) {
		mJmDNS.addServiceListener(Constants.TYPE, mServiceListener = new ServiceListener() {
			
			@Override
			public void serviceResolved(ServiceEvent serviceEvent) {

			}
			
			@Override
			public void serviceRemoved(ServiceEvent serviceEvent) {
			}
			
			@Override
			public void serviceAdded(ServiceEvent serviceEvent) {
				ServiceInfo info = mJmDNS.getServiceInfo(serviceEvent.getType(), serviceEvent.getName());
				listener.onFound(info.getInetAddresses()[0], info.getPort());
			}
		});
		
	}
	
	public void reset() {
		if (mJmDNS != null) {
			if (mServiceListener != null) {
				mJmDNS.removeServiceListener(Constants.TYPE, mServiceListener);
				mServiceListener = null;
			}
			mJmDNS.unregisterAllServices();
		}
	}
	
	public static interface OnFoundListenerAction {
		public void onFound(InetAddress address, int port);
	}
	
	
}
