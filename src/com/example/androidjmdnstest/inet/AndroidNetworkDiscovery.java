package com.example.androidjmdnstest.inet;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.jmdns.JmDNS;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class AndroidNetworkDiscovery extends NetworkDiscovery {
	private Context andContext = null;
	private WifiManager.MulticastLock andMulticastLock;
	
	public AndroidNetworkDiscovery(Context context) {
		andContext = context;
		WifiManager wifi = (WifiManager) andContext.getSystemService(android.content.Context.WIFI_SERVICE);
		WifiInfo wifiInfo = wifi.getConnectionInfo();
		int intaddr = wifiInfo.getIpAddress();

		byte[] byteaddr = new byte[]{(byte) (intaddr & 0xff), (byte) (intaddr >> 8 & 0xff), (byte) (intaddr >> 16 & 0xff), (byte) (intaddr >> 24 & 0xff)};
		InetAddress addr = null;
		try {
			addr = InetAddress.getByAddress(byteaddr);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		
		wifiLock();
		
		try {
			mJmDNS = JmDNS.create(addr);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	private void wifiLock() {
		WifiManager wifiManager = (WifiManager) andContext.getSystemService(android.content.Context.WIFI_SERVICE);
		andMulticastLock = wifiManager.createMulticastLock(Constants.SERVICE_NAME);
		andMulticastLock.setReferenceCounted(true);
		andMulticastLock.acquire();
	}
	
}

